use Purple;

my $login = "mcepl";
my $password = "kyrios";
my $loggedAccount = "mcepl\@porky.stuttgart.redhat.com";

my %loggedInServers = ();

# 	send-message("$botname $logins{$server} $passwds{$server}")
my %loggins = ();
my %passwds = ();

$botname = "USERSERV";
%PLUGIN_INFO = (
    perl_api_version => 2,
    name => "Perl: Login to $botname",
    version => "0.2",
    summary => "Logs to $botname on RH IRC",
    description => "Send login to $botname on login to the IRC account.",
    author => "Matěj Cepl <mcepl\@redhat.com>",
    url => "http://matej.ceplovi.cz",
    load => "plugin_load",
    unload => "plugin_unload",
);

#(00:25:06) charding: Can anyone tell me what's the difference
#between the 'signing-on' and 'account-connecting' signals? Is
#signing-on for when pidgin is starting up? and account-connecting
#is when pidgin is already running and you enable and that
#particular acct is connecting?
#(00:29:18) elb: charding: as best I can tell, the only difference is that
#signing-on is called for both registration and regular
#connection, and account-connecting is called only for the later
#(00:29:22) elb: latter
#(00:29:28) elb: they're emitted one right after another in
#purple_connection_new


sub plugin_load {
	Purple::Debug::info("loginRHIRC", "start paramterers=@_\n");
	$plugin = shift;
	
	$conn = Purple::Connections::get_handle();
	Purple::Signal::connect($conn, "signed-on", $plugin,
		\&signed_on, 0);
	Purple::Signal::connect($conn, "chat-joined", $plugin,
		\&joined_chatroom, 0);
	Purple::Debug::info("loginRHIRC", "Login to $botname plugin loaded\n");
}

sub plugin_unload {
	Purple::Debug::info("loginRHIRC", "Login to $botname plugin unloaded\n");
}

sub signed_on {
	my $conn = shift;
	my $account = $conn->get_account();
	my $username = $account->get_username();
	Purple::Debug::misc("loginRHIRC", "signed-on (" . $username . ")\n");
	
	# login to USERSERV
	if ($username eq $loggedAccount) {
	    my $ret = Purple::Prpl::send_raw($conn,
	        ":$login PRIVMSG $botname :login $login $password\n");
	    Purple::Debug::misc("loginRHIRC", "ret=$ret\n");
	}

# 	Purple::timeout_add();
    sleep(10);
	
    my @allChats = Purple::get_ims();
    my @allTitles = map($_->get_title(),@allChats);
    Purple::Debug::misc("loginRHIRC", "\@allTitles=@allTitles\n");
    Purple::Debug::misc("loginRHIRC", "Purple::get_ims() = " . Purple::get_ims());
	my @userservChats = grep ((
	        $_->get_title() =~ /^\W*$botname\W*$/
	    ), Purple::get_ims());
    $userservChats[1]->destroy();
}

sub joined_chatroom {
	my $conn = shift; # *PurpleConversation
	my $IRCserver = "";
	my $IRCgateway = "";
	my $account = $conn->get_account();
	my $identificator = $account->get_username();
	my $proto = $account->protocol_id();
	Purple::Debug::misc("loginRHIRC", "opened $identificator, with protocol = $proto.\n");

	# if server(MUC-JID)	in nickservs:
	#      send-message("Nickserv!$server@IRCgateway",
	#             "identify $passwds{$server}")
	# elif server(MUC-JID)	in userservs:
	#      send-message("$botname $logins{$server} $passwds{$server}")
}
